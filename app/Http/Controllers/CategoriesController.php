<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Laracasts\Flash\Flash;
use App\Http\Requests\CategoryRequest;
class CategoriesController extends Controller
{
     public function index()
    {
     $categories= Category::orderBy('id','DESC')->paginate(5);	
      return view('admin.categories.index',['categories' =>$categories]);

    }


    public function create()
    {
        #metodo que llama a la vista del formulario de ingreso
       
    	return view('admin.categories.create');#vistal del nombre del metodo que la llama o usa
    }

    public function store(CategoryRequest $request)
    {
        #metodo que inserta un usuario en la base de datos
    	$category= new Category($request->all());
    	
    	$category->save();
    	
        Flash::success("Se ha registrado a ".$category->name." de manera exitosa!");
        return redirect()->route('categories.index');
    }


    public function show($id)
    {

    }

    public function edit($id)
    {
      $category = Category::find($id);
        return view ('admin.categories.edit',['category' => $category]);

    }

    public function update(Request $request, $id)
    {
        $category=category::find($id);
        $category->name = $request->name;
        /*es igual a lo de abajo el metodo fill reemplaza los atributos del objeto $request en el objeto $user*/
        $category->save();

        Flash::warning('La categoría '.$category->name.' de ID '.$category->id.' ha sido modificado exitosamente!');
        return redirect()->route('categories.index');
    }

    public function destroy($id)
    {
    	$category = Category::find($id);
        $category->delete();

        flash:: error('La categoría '.$category->name.' de ID '.$category->id.' a sido eliminado exitosamente!');

        return redirect()->route('categories.index');
    }
       
}
