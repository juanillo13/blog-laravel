@extends('admin.main')

@section('title','Lista de usuarios')

@section('content')
<div class="container">
	<a href="{{ route('users.create')}}" class="btn btn-info">Agregar nuevo usuario</a>
</div>
</br>
	<table class="table table-bordered">
	  <thead>
	  		<th>ID</th>
	  		<th>Nombre</th>
	  		<th>Correo</th>
	  		<th>Tipo</th>
	  		<th>Acción</th>
	  </thead>
	  <tbody>
	  		@foreach($users as $user)
	  			<tr>
	  				<td>{{ $user->id }}</td>
	  				<td>{{ $user->name }}</td>
	  				<td>{{ $user->email }}</td>
	  				<td>
						<h4><span class="label label-danger">{{ $user->type }}</span></h4>
	  				</td>

	  				<td> 
	  					<a href="{{ route('users.edit', $user->id) }}" class="btn btn-success">Editar</a>
	  					<a href="{{ route('users.destroy', $user->id) }}" onclick ="return confirm('¿Seguro que desea elminiar este registro?')" class="btn btn-danger" >Eliminar</a>
	  					
	  				</td>
	  			</tr>

	  		@endforeach
	  </tbody>
	</table>
	{{ $users->render() }}<!--es para a paginación-->
@endsection