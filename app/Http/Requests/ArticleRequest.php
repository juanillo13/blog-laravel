<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'min:8|max:300|required|unique:articles',
            'content' => 'required',
            'category_id' => 'required',
            'image' => 'image|required' 
            #la validación image resfringe para que el archivo a subir sea una imagen
        ];
    }
}
