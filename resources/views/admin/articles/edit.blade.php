@extends('admin.main')

@section('title','Editar Artículo '.$article->title)

@section('content')

	{!! Form::open(['route' => ['articles.update',$article], 'method' => 'PUT']) !!}
		<div class="form-group">
			{!! Form::label('title','Título: ')!!}
			{!! Form::text('title',$article->title,['class' => 'form-control', 'required'])!!}

		</div>

		<div class="form-group">
			{!! Form::label('content','Cuerpo: ')!!}
			{!! Form::textarea('content',$article->content,['class' => 'form-control textarea','required'])!!}

		</div>
		<div class="form-group">
			{!! Form::label('category_id', 'Categorias') !!}
			{!! Form::select('category_id', $categories,$article->category->id,['class' => 'form-control','required'])!!}
			

		</div>

		<div class="form-group">
			{!! Form::label('tags', 'tags') !!}
			{!! Form::select('tags[]', $tags,$my_tags,['class' => 'form-control chosen-select','multiple','required'])!!}
		</div>

		<div class="form-group">
			
			{!! Form::submit('Editar',['class' => 'btn btn-success'])!!}

		</div>		

	{!! Form::close() !!}
@endsection

@section('js')

<script type="text/javascript">
	
	$(".chosen-select").chosen({

		placeholder_text_multiple: 'selecciones un máximo de 3 tags',
		max_selected_options: 3,
		no_results_text: "No se encontraron tags!!"
	});
</script>
<script type="text/javascript">
	$(".textarea").trumbowyg({
    	lang: 'es'
});

</script>


@endsection