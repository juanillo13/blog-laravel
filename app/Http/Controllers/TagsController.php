<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tag;
use Laracasts\Flash\Flash;
use App\Http\Requests\TagRequest;

class TagsController extends Controller
{
    public function index(Request $request)
    {
        #funcionalidad del scope.. que es una busqueda 
    	$tags= Tag::search($request->name)->orderBy('id','DESC')->paginate(5);	
      	return view('admin.tags.index',['tags' =>$tags]);

    }


    public function create()
    {
        #metodo que llama a la vista del formulario de ingreso
       
    	return view('admin.tags.create');#vistal del nombre del metodo que la llama o usa
    }

    public function store(TagRequest $request)
    {
    	#metodo que inserta un tag en la base de datos
    	$tag= new Tag($request->all());
    	
    	$tag->save();
    	
        Flash::success("Se ha registrado a ".$tag->name." de manera exitosa!");
        return redirect()->route('tags.index');
	}

    public function show($id)
    {

    }

    public function edit($id)
    {
    	$tag = Tag::find($id);
        return view ('admin.tags.edit',['tag' => $tag]);
    }

    public function update(Request $request, $id)
    {
    	$tag=Tag::find($id);
        $tag->name = $request->name;
        $tag->save();

        Flash::warning('El tag '.$tag->name.' de ID '.$tag->id.' ha sido modificado exitosamente!');
        return redirect()->route('tags.index');
    }

    public function destroy($id)
    {
    	$tag = Tag::find($id);
        $tag->delete();

        flash:: error('La categoría '.$tag->name.' de ID '.$tag->id.' a sido eliminado exitosamente!');

        return redirect()->route('tags.index');
    }
}
