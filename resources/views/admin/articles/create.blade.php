@extends('admin.main')

@section('title','Crear nuevo artículo')

@section('content')

	{!! Form::open(['route' => 'articles.store', 'method' => 'POST', 'files' => 'true']) !!}
		<div class="form-group">
			{!! Form::label('title','Título: ')!!}
			{!! Form::text('title',Null,['class' => 'form-control', 'placeholder' => 'Título del artículo', 'required'])!!}

		</div>

		<div class="form-group">
			{!! Form::label('content','Cuerpo: ')!!}
			{!! Form::textarea('content',Null,['class' => 'form-control textarea', 'placeholder' => 'Cuerpo del artículo...', 'required'])!!}

		</div>
		<div class="form-group">
			{!! Form::label('category_id', 'Categorias') !!}
			{!! Form::select('category_id', $categories,NULL,['class' => 'form-control','placeholder' => 'Seleccione una opción...','required'])!!}
			

		</div>

		<div class="form-group">
			{!! Form::label('tags', 'tags') !!}
			{!! Form::select('tags[]', $tags,NULL,['class' => 'form-control chosen-select','multiple','required'])!!}
		</div>

		<div class="form-group">
			{!! Form::label('image','Imagen')!!}
			{!! Form::file('image')!!}

		</div>

		<div class="form-group">
			
			{!! Form::submit('Agregar',['class' => 'btn btn-success'])!!}

		</div>		

	{!! Form::close() !!}
@endsection

@section('js')

<script type="text/javascript">
	
	$(".chosen-select").chosen({

		placeholder_text_multiple: 'selecciones un máximo de 3 tags',
		max_selected_options: 3,
		no_results_text: "No se encontraron tags!!"
	});
</script>
<script type="text/javascript">
	$(".textarea").trumbowyg({
    	lang: 'es'
});

</script>


@endsection
