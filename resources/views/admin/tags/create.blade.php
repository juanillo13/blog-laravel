@extends('admin.main')


@section('title','Crear un nuevo tags')
@section('content')


	{!! Form::open(['route' => 'tags.store', 'method' => 'POST']) !!}
		<div class="form-group">
			{!! Form::label('name','Nombre: ')!!}
			{!! Form::text('name',Null,['class' => 'form-control', 'placeholder' => 'Título del Tag', 'required'])!!}

		</div>


		<div class="form-group">
			
			{!! Form::submit('Agregar',['class' => 'btn btn-success'])!!}

		</div>		

	{!! Form::close() !!}

@endsection