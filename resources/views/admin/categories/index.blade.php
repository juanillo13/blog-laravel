
@extends('admin.main')

@section('title','Lista de Categorías')

@section('content')
<div class="container">
	<a href="{{ route('categories.create')}}" class="btn btn-info">Agregar nueva Categoria</a>
</div>
</br>
	<table class="table table-bordered">
	  <thead>
	  		<th class="col-sm-2">ID</th>
	  		<th class="col-sm-8">Nombre</th>
	  		<th class="col-sm-2">Acción</th>
	  </thead>
	  <tbody>
	  		@foreach($categories as $category)
	  			<tr>
	  				<td>{{ $category->id }}</td>
	  				<td>{{ $category->name }}</td>
	  				<td> 
	  					<a href="{{ route('categories.edit', $category->id) }}" class="btn btn-success">Editar</a>
	  					<a href="{{ route('categories.destroy', $category->id) }}" onclick ="return confirm('¿Seguro que desea elminiar este registro?')" class="btn btn-danger" >Eliminar</a>
	  					
	  				</td>
	  			</tr>

	  		@endforeach
	  </tbody>
	</table>
	<div class="text-center">
	{{ $categories->render() }}<!--es para a paginación-->
	</div>
@endsection