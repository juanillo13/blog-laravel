@extends('admin.main')


@section('title','Crear una nueva categoría')
@section('content')


	{!! Form::open(['route' => 'categories.store', 'method' => 'POST']) !!}
		<div class="form-group">
			{!! Form::label('name','Nombre: ')!!}
			{!! Form::text('name',Null,['class' => 'form-control', 'placeholder' => 'Nombre completo', 'required'])!!}

		</div>


		<div class="form-group">
			
			{!! Form::submit('Agregar',['class' => 'btn btn-success'])!!}

		</div>		

	{!! Form::close() !!}

@endsection