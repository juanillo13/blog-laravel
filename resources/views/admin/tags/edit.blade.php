@extends('admin.main')

@section('title','Editar Tag '.$tag->name)

@section('content')
<!--aquí va a ir el formulario-->


	{!! Form::open(['route' => ['tags.update',$tag], 'method' => 'PUT']) !!}
		
		<div class="form-group">
			{!! Form::label('name','Nombre')!!}
			{!! Form::text('name',$tag->name,['class' => 'form-control','required'])!!}
		</div>

		<div class="form-group">
			{!! Form::submit('Editar',['class' => 'btn btn-success'])!!}
		</div>		
				

	{!! Form::close() !!}

@endsection