<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $table = "tags";
    protected $fillable = ['name'];


    public function articles()
    {
    	return $this->belongsToMany('App\Article')->withTimestamps();
    }

    #función que permite crear un buscador usando SCOPE
    public function scopeSearch($query, $name) #segundo parámetro es el que se manda

    {
    	return $query->where('name','LIKE',"%$name%");

    }
        public function scopeSearchTag($query, $name)
    {

        return $query->where('name','=',$name);
    }
}
