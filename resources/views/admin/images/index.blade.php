@extends('admin.main')

@section('title', 'Lista de Imágenes')

@section('content')
	<div class="row">
		@foreach($images as $image)
			<div class="col-md-4">
					<div class="panel panel-default">
				 		<div class= "panel-body">
							<img src="../images/articles/{{ $image->name }}" class="image-responsive" width="315" height="210">
						</div>
					<div class="panel-footer" align="center">{{ $image->article->title }}</div>
					</div>
			</div>
		@endforeach
	</div>
@endsection
