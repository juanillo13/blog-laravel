<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;


class Article extends Model

{   
    use Sluggable;

    public function sluggable() /*esta funcion me setea el campo titulo de la tabla articles en un slug de la forma hola-soy-un-slugg NOTA: los slugg sirven para las rutas*/
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
    protected $table = "articles";
    protected $fillable = ['title','content','user_id','category_id','slug'];

    public function category()
    {
    	return $this->belongsTo('App\Category'); #la referencia hacia el modelo
    }

    public function user()
    {
    	return $this->belongsTo('App\User'); #la referencia hacia el modelo
    }

    public function images()
    {
    	return $this->hasMany('App\Image');

    }

    public function tags()
    {
    	return $this->belongsToMany('App\Tag')->withTimestamps();
    }

       public function scopeSearch($query, $title) #segundo parámetro es el que se manda

    {
        return $query->where('title','LIKE',"%$title%");

    }
}
