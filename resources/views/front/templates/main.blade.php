<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Página principal</title>
    <link rel="stylesheet" type="text/css" href="{{asset('bootstrap/plugins/css/bootstrap.css')}}">
     <link href="css/bootstrap.min.css" rel="stylesheet">
    <link  href="{{asset('bootstrap/plugins/css/font-awesome.min.css')}}" rel="stylesheet">
    <link  href="{{asset('bootstrap/plugins/css/lightbox.css')}}" rel="stylesheet">
    <link  href="{{asset('bootstrap/plugins/css/animate.min.css')}}" rel="stylesheet">
   	<link  href="{{asset('bootstrap/plugins/css/main.css')}}" rel="stylesheet">
   	<link  href="{{asset('bootstrap/plugins/css/responsive.css')}}" rel="stylesheet">



    <script type="text/javascript" src="{{ asset('jquery/jquery-3.2.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bootstrap/plugins/js/bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bootstrap/plugins/js/lightbox.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bootstrap/plugins/js/wow.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bootstrap/plugins/js/main.js') }}"></script>

</head>
<body>

	<header>
		@include('front.templates.header')
	
	</header>


	<section>
		
	<div class="container">

		@yield('content')

	</div>
		
	</section>

	
 </body>
</html>

<style type="text/css">
	section{


	}
</style>
