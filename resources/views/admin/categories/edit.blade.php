@extends('admin.main')

@section('title','Editar Categoría '.$category->name)

@section('content')
<!--aquí va a ir el formulario-->


	{!! Form::open(['route' => ['categories.update',$category], 'method' => 'PUT']) !!}
		
		<div class="form-group">
			{!! Form::label('name','Nombre')!!}
			{!! Form::text('name',$category->name,['class' => 'form-control','required'])!!}
		</div>

		<div class="form-group">
			{!! Form::submit('Editar',['class' => 'btn btn-success'])!!}
		</div>		
				

	{!! Form::close() !!}

@endsection