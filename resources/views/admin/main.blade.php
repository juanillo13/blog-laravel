<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>hola</title>
    <link rel="stylesheet" type="text/css" href="{{asset('bootstrap/plugins/css/bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('chosen/chosen.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('trumbowyg/dist/ui/trumbowyg.min.css')}}">

    <script type="text/javascript" src="{{ asset('jquery/jquery-3.2.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bootstrap/plugins/js/bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('chosen/chosen.jquery.js') }}"></script>
    <script type="text/javascript" src="{{ asset('trumbowyg/dist/trumbowyg.js') }}"></script>
    <script type="text/javascript" src="{{ asset('trumbowyg/dist/langs/es.min.js') }}"></script>
    
</head>
<body>


	<nav>
		<div class="container">
		@include('admin.nav')<!--estoy incluyendo un nav bar de otra vista llamada nav.blade.php  
							que se encuentra en la carpeta admin,esto se llaman parciales-->
		</div>
	</nav>
	<section>
		<div class="container">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h4 align="center"> @yield('title')</h4>
				</div>
				<div class="panel-body">
						@include('flash::message')
						@include('admin.errors')
	     				@yield('content')
	     		</div>

	     	</div>
	    </div>
	</section>
	

	<footer >
			<div class="container">
		    	<nav class="navbar navbar-default navbar">
  		    		<div class="container">
  		    			<div class="collapse navbar-collapse">
  		    				<p class="navbar-text">Utilización de laravel 5.4 {{ date('Y') }}
  		    				</p>
  		    				<p class="navbar-text navbar-right">Juan Espinosa</p>
  		    						
  		    			</div>
  		    		</div>
		    	</nav>
		    </div>
		
	</footer>
	@yield('js')
 </body>
</html>
