<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User; #importando el modelo User
use Illuminate\Support\Facades\DB;
use Laracasts\Flash\Flash;
use App\Http\Requests\UserRequest;

class UsersController extends Controller
{
    public function index()
    {
        #estara el crud y será como la vista de gestión 
        #primero hay que traer los datos de la bd y guardarlos en una variable
        $users= User::orderBy('id','DESC')->paginate(5);
        return view('admin.users.index',['users' => $users]);

    }


    public function create()
    {
            #metodo que llama a la vista del formulario de ingreso
    	return view('admin.users.create');#vistal del nombre del metodo que la llama o usa
    }

    public function store(UserRequest $request)
    {
        #metodo que inserta un usuario en la base de datos
    	$user= new User($request->all());
    	$user->password= bcrypt($request->password);
    	$user->save();
    	
        Flash::success("Se ha registrado a ".$user->name." de manera exitosa!");
        return redirect()->route('users.index');
    }


    public function show($id)
    {

    }

    public function edit($id)
    {
        $user = User::find($id);
        //dd($user);
        return view ('admin.users.edit',['user' => $user]); #redireccionamos a la vista edit 

    }

    public function update(Request $request, $id)
    {
        $user=User::find($id);
        /*$user->$fill($request->all()); es igual a lo de abajo el metodo fill reemplaza los atributos del objeto $request en el objeto $user*/
        $user->name = $request->name;
        $user->email = $request->email;
        $user->type = $request->type;
        $user->save();

        Flash::warning('El usuario '.$user->name.' ha sido modificado exitosamente!');
        return redirect()->route('users.index');
    }

    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        flash:: error('el usuario '.$user->name.' y de ID '.$user->id.' a sido eliminado exitosamente!');

        return redirect()->route('users.index');
    }
    /*qué es request: toma los datos del formulario y los manda a un objeto request
         se puede crear propios request se pueden validar los campos a recivir por un método
    */
}
