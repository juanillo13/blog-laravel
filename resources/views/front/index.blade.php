@extends('front.templates.main')

@section('content')

<hr>


   <section>
   	<h2>Artículos</h2>
   </section>
    <!--/#action-->

    <section id="blog" class="padding-top">
        <div class="container">
            <div class="row">
    
                <div class="col-md-9 col-sm-7">
                
                    <div class="row">
                         <div class="col-md-6 col-sm-12 blog-padding-right">
                         @foreach($articles as $article)
                            <div class="single-blog two-column">
                                <div class="post-thumb">
                                @foreach($article->images as $images)
                                    <a href=""><img src="{{ asset('/images/articles/' . $images->name) }}" class="img-responsive" alt=""></a>
                                    @endforeach
                                </div>
                                <div class="post-content overflow">
                                    <h2 class="post-title bold"><a href="{{route('view.article',$article->slug)}}">{{ $article->title}}</a></h2>
                                    <h3 class="post-author"><a href="#">{{ $article->user->name}}</a></h3>
                                    <p>{{$article->content}}</p>
                                    <a href="#" class="read-more">View More</a>

                                    <div class="post-bottom overflow">
                                        <ul class="nav nav-justified post-nav">
                                            <li><a href="#"><i class=""></i> Categoría: {{ $article->category->name}}</a></li>
                                            <li><a href="#"><i class=""></i>Posteado: {{$article->created_at->diffForHumans() }}</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                             @endforeach
                        </div>

                    </div>
                   
                 </div>
                <div class="col-md-3 col-sm-5">
                    <div class="sidebar blog-sidebar">
                       
                        <div class="sidebar-item categories">
                            <h3>Categorías</h3>
                            <ul class="nav navbar-stacked">
                            @foreach($categories as $category)
                                <li><a href="{{ route('search.category', $category->name) }}">{{$category->name}}<span class="pull-right">
                                	{{$category->articles->count()}}
                                </span></a></li>
                              @endforeach
                            </ul>
                        </div>
                        <div class="sidebar-item tag-cloud">
                            <h3>Tags</h3>
                            <ul class="nav nav-pills">
                            	@foreach($tags as $tag)
                                <li><a href="{{ route('search.tag', $tag->name)}}">{{ $tag->name }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                            <div class="sidebar-item tag-cloud">
                            <h3>Administración</h3>
                            <ul class="nav nav-pills">
                            	
                                <li><a href="{{ route('login')}}">Iniciar Sesión</a></li>
                                
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{ $articles->render() }}
    <!--/#blog-->

    <footer id="footer">
        
    </footer>
@endsection