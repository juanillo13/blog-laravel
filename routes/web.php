<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', function () {
    return view('auth/login');
})->name('login');

/*Route::get('/hola/{nombre}',function ($nombre){

	echo "hola, esto es una prueba ".$nombre;
});

Route::get('/prueba/{nombre?}',function ($nombre="no hay nombre"){

	echo "hola esto es una prueba ".$nombre;
});

Route::group(['prefix' => 'article'],function(){

	Route::get('/view/{id}',[

		'uses' => 'TestController@view',#nombrecontrolador@metodo
		'as'   => 'articleView'
		]);


});*/

Route::group(['prefix' => 'admin','middleware'=>'auth'],function(){

	Route::resource('users','UsersController');
	Route::get('users/{id}/destroy', 'UsersController@destroy')->name('users.destroy');
	#forma de hacer el as => nombre en laravel 5.4

	Route::resource('categories','CategoriesController');
	Route::get('categories/{id}/destroy', 'CategoriesController@destroy')->name('categories.destroy'); #es para crear una ruta de de tipo DELETE para poder eliminar un registro de la base de datos

	Route::resource('tags','TagsController');
	Route::get('tags/{id}/destroy', 'tagsController@destroy')->name('tags.destroy');

	Route::resource('articles','ArticlesController');
	Route::get('articles/{id}/destroy', 'ArticlesController@destroy')->name('articles.destroy');

	Route::get('images','ImagesController@index')->name('images.index');


});


Auth::routes();



Route::get('/home', 'HomeController@index')->name('home');

#rutas del front
#funcion raiz del proyecto nombre/public/

Route::get('/','FrontController@index')->name('front.index');

Route::get('categories/{name}','FrontController@searchCategory')->name('search.category');

Route::get('tags/{name}','FrontController@searchTag')->name('search.tag');

Route::get('articles/{slug}','FrontController@viewArticle')->name('view.article');
