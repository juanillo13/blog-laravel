@extends('front.templates.main')


@section('content')
	<body>

    <!--/#page-breadcrumb-->

    <section id="blog-details" class="padding-top">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-7">
                    <div class="row">
                         <div class="col-md-12 col-sm-12">
                            <div class="single-blog blog-details two-column">
                                <div class="post-thumb">
                                	@foreach($article->images as $images)
                                    <a href="#"><img src="{{ asset('/images/articles/' . $images->name) }}" class="img-responsive" alt=""></a>
                                    @endforeach
                                    <div class="post-overlay">
                                        <span class="uppercase"><a href="#">14 <br><small>Feb</small></a></span>
                                    </div>
                                </div>
                                
                                <div class="post-content overflow">
                                    <h2 class="post-title bold"><a href="#">{{$article->title}}</a></h2>
                                    <h3 class="post-author"><a href="#">Posteado por: {{ $article->user->name}}</a></h3>
                                    <p> {{$article->content}} </p>
                                    <div class="post-bottom overflow">
                                        <ul class="nav navbar-nav post-nav">
                                            <li><a href="#"></i>Categoría: {{$article->category->name}}</a></li>
                                            <li><a href="#"></i>Posteado: {{$article->created_at->diffForHumans() }}</a></li>
                                          
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                 </div>
                <div class="col-md-3 col-sm-5">
                    <div class="sidebar blog-sidebar">
                        <div class="sidebar-item  recent">
                            
    
                        </div>
                        <div class="sidebar-item categories">
                            <h3>Categorías</h3>
                            <ul class="nav navbar-stacked">
                             @foreach($categories as $category)
                                <li><a href="{{ route('search.category', $category->name) }}">{{$category->name}}<span class="pull-right">{{$category->articles->count()}}</span></a></li>
                              @endforeach  
                        </div>
                        <div class="sidebar-item tag-cloud">
                            <h3>Tags</h3>
                            <ul class="nav nav-pills">
                                @foreach($tags as $tag)
                                <li><a href="{{ route('search.tag', $tag->name)}}">{{ $tag->name }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                         <div class="sidebar-item tag-cloud">
                            <h3>Administración</h3>
                            <ul class="nav nav-pills">
                            	
                                <li><a href="{{ route('login')}}">Iniciar Sesión</a></li>
                                
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
    	<div class="container"> 
    	<h2> Comentarios</h2>
    		<div id="disqus_thread"></div>
				<script>

				/**
				*  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
				*  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
				/*
				var disqus_config = function () {
				this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
				this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
				};
				*/
				(function() { // DON'T EDIT BELOW THIS LINE
				var d = document, s = d.createElement('script');
				s.src = 'https://blog-pruebalaravel.disqus.com/embed.js';
				s.setAttribute('data-timestamp', +new Date());
				(d.head || d.body).appendChild(s);
				})();
				</script>
				<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
			</div>

    </section>

@endsection