@extends('admin.main')

@section('title','Crear nuevo artículo')

@section('content')
<a href="{{ route('articles.create')}}" class="btn btn-info">Agregar nuevo Articulo</a>

{!! Form::open(['route' => 'articles.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}-

	<div class="input-group ">
		{!! Form::Text('title',NULL, ['class'=> 'form-control', 'placeholder' => 'Buscar artículos...', 'aria-describedby' =>'search']) !!}

		<span class="input-group-addon" id="search"><span class="glyphicon glyphicon-search"></span></span>
	</div>
	

{!! Form::close() !!}
<hr>
</br>
	<table class="table table-bordered">
	  <thead>
	  		<th>ID</th>
	  		<th class="col-sm-2">Titulo</th>
	  		<th class="col-sm-4">Contenido</th>
	  		<th>Categoría</th>
	  		<th>Usuario</th>
	  		<th class="col-sm-2">Acción</th> <!-- class="col-sm-2" cambiar ancho de la columna-->
	  </thead>
	  <tbody>
	  		@foreach($articles as $article)
	  			<tr>
	  				<td>{{ $article->id }}</td>
	  				<td>{{ $article->title }}</td>
	  				<td>{{ $article->content}}</td>
	  				<td>{{ $article->category->name}}</td> <!--esto se puede hacer donde hago el each(function($articles){

    						$articles->category;
    						$articles->user; en el controlador ArticlesController-->
	  				<td>{{ $article->user->name}}</td>
	  				<td> 
	  					<a href="{{ route('articles.edit', $article->id)}}" class="btn btn-success">Editar</a>
	  					<a href="{{ route('articles.destroy', $article->id)}}" onclick ="return confirm('¿Seguro que desea elminiar este registro?')" class="btn btn-danger" >Eliminar</a>
	  					
	  				</td>
	  			</tr>

	  		@endforeach
	  </tbody>
	</table>
	<div class="text-center">
	{{ $articles->render() }}<!--es para a paginación-->
	</div>
@endsection