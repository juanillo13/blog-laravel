
@extends('admin.main')

@section('title','Lista de Tags')

@section('content')

	<a href="{{ route('tags.create')}}" class="btn btn-info">Agregar nuevo Tag</a>

{!! Form::open(['route' => 'tags.index', 'method' => 'GET', 'class' => 'navbar-form pull-right']) !!}

	<div class="input-group ">
		{!! Form::Text('name',NULL, ['class'=> 'form-control', 'placeholder' => 'Buscar tag..', 'aria-describedby' =>'search']) !!}

		<span class="input-group-addon" id="search"><span class="glyphicon glyphicon-search"></span></span>
	</div>
	

{!! Form::close() !!}
<hr>
</br>
	<table class="table table-bordered">
	  <thead>
	  		<th class="col-sm-2">ID</th>
	  		<th class="col-sm-8">Nombre</th>
	  		<th class="col-sm-2">Acción</th>
	  </thead>
	  <tbody>
	  		@foreach($tags as $tag)
	  			<tr>
	  				<td>{{ $tag->id }}</td>
	  				<td>{{ $tag->name }}</td>
	  				<td> 
	  					<a href="{{ route('tags.edit', $tag->id) }}" class="btn btn-success">Editar</a>
	  					<a href="{{ route('tags.destroy', $tag->id) }}" onclick ="return confirm('¿Seguro que desea elminiar este registro?')" class="btn btn-danger" >Eliminar</a>
	  					
	  				</td>
	  			</tr>

	  		@endforeach
	  </tbody>
	</table>
	<div class="text-center">
	{{ $tags->render() }}<!--es para a paginación-->
	</div>
@endsection