<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Category;
use App\Tag;
use Carbon\carbon; #paquete que permite interactuar con las fechas , buscado desde packagist.io


class FrontController extends Controller
{

	public function __construct()

	{

		Carbon::setLocale('es'); #seteo el idioma de carbon a español, lo hago en un contructor de la clase FrontController porque así extiende a todos los métodos y me ahorro el estar seteando la clase carbon cada vez que la utilice. 
	}

   	public function index ()
   	{
   		$articles=Article::orderBy('id','DESC')->paginate(5);
   		$categories = Category::orderBy('id','DESC')->get();
   		$tags = Tag::orderBy('id','DESC')->get();

   		$articles->each(function($articles){
   			$articles->category;
   			$articles->user; #llamo a las relaciones que hice antes en el modelo +
   			$articles->images;
   		});
   		return view ('front.index',['articles' => $articles, 'categories' => $categories, 'tags' => $tags]);
   	}


   	public function searchCategory($name)
   	{
   		$tags = Tag::orderBy('id','DESC')->get();

   		$categories = Category::orderBy('id','DESC')->get();
   		$category = Category::searchCategory($name)->first();
   		$articles = $category->articles()->paginate(5);
   		$articles->each(function($articles){

   			$articles->category;
   			$articles->images;

   		});


   		return view ('front.index',['articles' => $articles, 'categories' => $categories, 'tags' => $tags]);

   	}



   	public function searchTag($name)
   	{
   		$tags = Tag::orderBy('id','DESC')->get();

   		$categories = Category::orderBy('id','DESC')->get();
   		$tag = Tag::SearchTag($name)->first();
   		$articles = $tag->articles()->paginate(5);
   		$articles->each(function($articles){

   			$articles->category;
   			$articles->images;

   		});


   		return view ('front.index',['articles' => $articles, 'categories' => $categories, 'tags' => $tags]);

   	}

   	public function viewArticle($slug)

   	{
   		#$article= Article::findBySlugOrFail($slug);
   		$article = Article::where('slug', $slug)->firstOrFail(); #esta funcion es para obtener el articlo del slug que le estamos pasando como parámetro
   		$article->category;
   		$article->user;
   		$article->tags;
   		$article->images;
   		$categories = Category::orderBy('id','DESC')->get();
   		$tags = Tag::orderBy('id','DESC')->get();


   		return view('front.article',['article' => $article,'categories' => $categories, 'tags' => $tags]);

   	}
   	
}
