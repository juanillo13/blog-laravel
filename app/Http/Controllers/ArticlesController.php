<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Tag;
use App\Article; #modelo Article
use App\Image;
use Laracasts\Flash\Flash;
use App\Http\Requests\ArticleRequest;

class ArticlesController extends Controller
{
    public function index(Request $request)
    {
    	$articles= Article::search($request->title)->orderBy('id','DESC')->paginate(5);
    	#llamar relaciones each hace un recorrido por cada de uno de los articulos, despues llamo a las relaciones para obtener los datos de la categoria y el usuario del articulo.
    	$articles->each(function($articles){

    		$articles->category;
    		$articles->user;

    	});
    	
    	return view('admin.articles.index',['articles'=> $articles]);

    }


    public function create()
    {
        #metodo que llama a la vista del formulario de ingreso

        $categories = Category::orderBy('name','ASC')->pluck('name','id'); #traer a todoas las categorias 
        #pluck es un método que permite listar un objeto por los parametros que se le pasen.
        
        $tags = Tag::orderBy('name','ASC')->pluck('name','id'); #trae a todos los tags
       
    	return view('admin.articles.create',['categories' =>$categories, 'tags' =>$tags]);
    	#vista del nombre del metodo que la llama o usa
    }

    public function store(ArticleRequest $request)
    {
    	#método para insertar datos en la BD
    	//manipulación de imágenes|archivos
    	if ($request->file('image')) {
    		   	$file=$request->file('image');
    			$namefile= 'blog_'.time().'.'.$file->getClientOriginalExtension();
    			$path= public_path().'\images\articles';
    	#la funcion public_path() nos trae la dirección de donde se encuentra el proyecto
    			$file->move($path,$namefile);
    	#C:\xampp\htdocs\pruebalaravel\public\images\articles direccion del directorio en el compu
    	}

    	$article = new Article($request->all());#guardo toda la info del formulario en un objeto article
    	$article->user_id=\Auth::user()->id; #igualamos el id del articulo a guardar en la BD con el id del usuario que se ha autentificado. por eso llamamos a la clase AUTH 
    	$article->save(); #guardamos el articulo en la BD con el método save()

    	$article->tags()->sync($request->tags); #primero llamamos al método tags() de nuestro modelo article, el metodo tags tiene las relaciones entre las tablas en la bd. segundo usando la funcion sync que rellena la tabla pivote (relación muchos a muchos) y le pasamos como parámetro los tags que obtenmos mediante un request (los tags son enviados desde la vista por un request)

    	$image = new Image();
    	$image->name = $namefile;#guardo en el nombre de la imagen la ruta en donde se guardará
    	$image->article()->associate($article);
    	#associate() obtiene la asociacion la imagen con la clave foranea que es la id del articulo, le pasamos el objeto completo. 
    	$image->save();

    	Flash::success('se ha creado el articulo ' . $article->title . ' de manera exitosa!!');
    	return redirect()->route('articles.index');
    }


    public function show($id)
    {

    }

    public function edit($id)
    {	
    	$article = Article::find($id);
    	$article->category; #llamar a la relacion en el modelo llamada category()

    	$categories= Category::orderBy('name','DESC')->pluck('name','id');
    	$tags = Tag::orderBy('name','DESC')->pluck('name','id');

    	$my_tags = $article->tags->pluck('id')->ToArray();
    	#en la variable my_tags estoy obteniendo todos los tags de un articulo ordenados por id y transformados de un objeto a un array con el metodo ToArray
    	
    	return view('admin.articles.edit',['categories'=> $categories, 'article' => $article,'my_tags' => $my_tags,'tags' => $tags]);
    }

    public function update(Request $request, $id)
    {
    	$article= Article::find($id);
    	$article->fill($request->all()); #hace el update de los campos por los enviados desde el formulario con request
    	$article->save();

    	$article->tags()->sync($request->tags);

    	Flash::warning('se ha editado el articulo '.$article->title.' de ID '.$article->id.' de forma exitosa!!!');

    	return redirect()->route('articles.index');
    }

    public function destroy($id)
    {
    	$article = Article::find($id);
    	$article->delete();

    	Flash::error('Se ha eliminado el artículo '. $article->title . ' de ID ' . $article->id . ' de forma exitosa!!!!');
    	return redirect()->route('articles.index');
    }
}
