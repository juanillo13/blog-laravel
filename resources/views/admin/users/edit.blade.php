@extends('admin.main')

@section('title','Editar usuario '.$user->name)

@section('content')
<!--aquí va a ir el formulario-->


	{!! Form::open(['route' => ['users.update',$user], 'method' => 'PUT']) !!}
		<div class="form-group">
			{!! Form::label('name','Nombre')!!}
			{!! Form::text('name',$user->name,['class' => 'form-control', 'placeholder' => 'Nombre completo', 'required'])!!}

		</div>

		<div class="form-group">
			{!! Form::label('email','Correo Electrónico')!!}
			{!! Form::email('email',$user->email,['class' => 'form-control', 'placeholder' => 'ejemplo@ejemplo.cl', 'required'])!!}

		</div>


		<div class="form-group">
			{!! Form::label('type','Tipo de Usuario')!!}
			{!! Form::select('type',['member' => 'Usuario Externo', 'admin' => 'Administrador de Sitio'],$user->type,['class'=>'form-control']) !!}

		</div>

		<div class="form-group">
			
			{!! Form::submit('Editar',['class' => 'btn btn-success'])!!}

		</div>		

	{!! Form::close() !!}

	@section('footer')
		<p aling="center"> Pruebas de laravel 5.4 </p>
	@endsection

@endsection