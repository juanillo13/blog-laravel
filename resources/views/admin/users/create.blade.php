@extends('admin.main')


@section('title','Crear un nuevo usuario')
@section('content')
<!--aquí va a ir el formulario-->


	{!! Form::open(['route' => 'users.store', 'method' => 'POST']) !!}
		<div class="form-group">
			{!! Form::label('name','Nombre')!!}
			{!! Form::text('name',Null,['class' => 'form-control', 'placeholder' => 'Nombre completo', 'required'])!!}

		</div>

		<div class="form-group">
			{!! Form::label('email','Correo Electrónico')!!}
			{!! Form::email('email',Null,['class' => 'form-control', 'placeholder' => 'ejemplo@ejemplo.cl', 'required'])!!}

		</div>

		<div class="form-group">
			{!! Form::label('password','Password')!!}
			{!! Form::password('password',['class' => 'form-control', 'placeholder' => '*****', 'required'])!!}

		</div>

		<div class="form-group">
			{!! Form::label('type','Tipo de Usuario')!!}
			{!! Form::select('type',['member' => 'Usuario Externo', 'admin' => 'Administrador de Sitio'],Null,['class'=>'form-control']) !!}

		</div>

		<div class="form-group">
			
			{!! Form::submit('Agregar',['class' => 'btn btn-success'])!!}

		</div>		

	{!! Form::close() !!}

@endsection